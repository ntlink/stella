package test

import (
	"crypto/rand"
	"encoding/hex"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/stella/pkg/dto"
)

func TestDto(t *testing.T) {
	tests := []struct {
		expect []byte
		actual []string
		err    error
		msg    string
	}{
		{
			expect: []byte("http://localhost/1\nhttp://localhost/2"),
			actual: []string{"http://localhost/1", "http://localhost/2"},
			msg:    "urls difference",
		},
	}

	t.Run("positive", func(t *testing.T) {
		t.Parallel()
		for _, study := range tests {
			lst, err := dto.NewList(study.expect)
			if err != nil {
				t.Error(err.Error())
			}
			assert.Equal(t, strings.Join(lst.List(), ","), strings.Join(study.actual, ","), study.msg)
			assert.Nil(t, lst.Validate())
		}
	})

	t.Run("negative", func(t *testing.T) {
		t.Run("url max length", func(t *testing.T) {
			t.Parallel()
			buff := make([]byte, 2048)
			_, _ = rand.Read(buff)
			str := "http://localhost/" + hex.EncodeToString(buff)
			lst, err := dto.NewList([]byte(str))
			assert.Nil(t, err)
			assert.NotNil(t, lst.Validate())
		})
		t.Run("empty body", func(t *testing.T) {
			t.Parallel()
			_, err := dto.NewList([]byte(""))
			assert.NotNil(t, err)
		})
		t.Run("incorrect url", func(t *testing.T) {
			t.Parallel()
			lst, err := dto.NewList([]byte("hello"))
			assert.Nil(t, err)
			assert.NotNil(t, lst.Validate())
		})
	})
}

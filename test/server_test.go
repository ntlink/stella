package test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"testing"
	"time"

	"gitlab.com/stella/pkg/controller"
	"gitlab.com/stella/pkg/dto"
	"gitlab.com/stella/pkg/usecase"
)

const (
	expect = 999
	count  = 1000
)

type task struct{}

func NewTask() usecase.Task {
	return &task{}
}

func (t *task) Processing(list dto.URLList) ([]byte, error) {
	<-time.After(time.Second)
	return []byte("1"), nil
}

//nolint
func TestMaxConn(t *testing.T) {
	tk := controller.NewRouter(NewTask())
	handler := controller.NewLimitHandler(expect)
	handler.HandleFunc("/", tk.HomeWork)
	ts := httptest.NewServer(handler)

	defer ts.Close()

	lst, _ := generatorURL(2)

	client := ts.Client()
	wg := &sync.WaitGroup{}

	t.Run("max", func(t *testing.T) {
		status := 0
		for i := 0; i < count; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()

				r, _ := http.NewRequest(http.MethodPost, ts.URL, strings.NewReader(lst))
				r.Header.Set("content-type", "text/plain")
				resp, err := client.Do(r)
				if err != nil {
					t.Error(err.Error())
				}
				if resp.StatusCode == http.StatusServiceUnavailable {
					status = http.StatusServiceUnavailable
				}
				defer resp.Body.Close()
			}()
		}

		wg.Wait()

		if status != http.StatusServiceUnavailable {
			t.Error("max connection return only OK")
		}
	})

	t.Run("normal", func(t *testing.T) {
		status := 0
		for i := 0; i < count-1; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				r, _ := http.NewRequest(http.MethodPost, ts.URL, http.NoBody)
				r.Header.Set("content-type", "text/plain")
				resp, err := client.Do(r)
				if err != nil {
					t.Error(err.Error())
				}
				if resp.StatusCode == http.StatusServiceUnavailable {
					status = http.StatusServiceUnavailable
				}

				defer resp.Body.Close()
			}()
		}

		wg.Wait()

		if status == http.StatusServiceUnavailable {
			t.Error("max connection error")
		}
	})
}

package test

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/stella/pkg/controller"
	"gitlab.com/stella/pkg/domain"
	"gitlab.com/stella/pkg/usecase"

	"github.com/stretchr/testify/assert"
)

type parser struct{}

func newParser() domain.Parser {
	return &parser{}
}

func (p *parser) Data(uri string) (string, error) {
	u, _ := url.Parse(uri)
	q := u.Query()
	index := q.Get("index")

	return index, nil
}

func TestHandler(t *testing.T) {
	log.SetFlags(log.Lshortfile)

	tk := controller.NewRouter(usecase.NewTask(newParser()))
	handler := controller.NewLimitHandler(expect)
	handler.HandleFunc("/", tk.HomeWork)
	ts := httptest.NewServer(handler)

	defer ts.Close()
	client := ts.Client()

	t.Run("parser", func(t *testing.T) {
		lst, expect := generatorURL(10)
		r, _ := http.NewRequest(http.MethodPost, ts.URL, bytes.NewBufferString(lst))
		r.Header.Set("content-type", "text/plain")
		res, err := client.Do(r)
		assert.Nil(t, err)
		body, err := io.ReadAll(res.Body)
		assert.Nil(t, err)
		defer res.Body.Close()
		assert.Equal(t, string(body), expect)
	})
}

func generatorURL(n int) (string, string) {
	u, _ := url.Parse("http://localhost")
	q := u.Query()
	list := make([]string, 0)
	expect := make([]string, 0)

	for i := 1; i <= n; i++ {
		q.Set("index", strconv.Itoa(i))
		u.RawQuery = q.Encode()
		list = append(list, u.String())
		expect = append(expect, strconv.Itoa(i))
	}

	return strings.Join(list, "\n"), strings.Join(expect, "\n")
}

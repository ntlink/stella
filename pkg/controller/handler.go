package controller

import (
	"net/http"
)

type LimitHandler interface {
	HandleFunc(pattern string, handler func(w http.ResponseWriter, r *http.Request))
	MiddleWare(next http.Handler) http.Handler
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
type limitHandler struct {
	router *http.ServeMux
	conn   chan struct{}
}

func NewLimitHandler(maxConns int) LimitHandler {
	handler := &limitHandler{
		router: http.NewServeMux(),
		conn:   make(chan struct{}, maxConns),
	}
	for i := 0; i < maxConns; i++ {
		handler.conn <- struct{}{}
	}

	return handler
}

func (h *limitHandler) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	select {
	case <-h.conn:
		h.router.ServeHTTP(writer, req)
		h.conn <- struct{}{}
	default:
		http.Error(writer, "503 Service Unavailable", http.StatusServiceUnavailable)
	}
}

func (h *limitHandler) HandleFunc(pattern string, handler func(writer http.ResponseWriter, request *http.Request)) {
	h.router.HandleFunc(pattern, handler)
}

func (h *limitHandler) MiddleWare(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodOptions {
			writer.Header().Add("Access-Control-Allow-Origin", "*")
			writer.Header().Add("Access-Control-Allow-Headers", "*")
			writer.Header().Add("Access-Control-Allow-Methods", "POST")
			return
		}
		next.ServeHTTP(writer, request)
	})
}

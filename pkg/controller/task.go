package controller

import (
	"io"
	"net/http"

	"gitlab.com/stella/pkg/dto"
	"gitlab.com/stella/pkg/usecase"
)

type Router interface {
	HomeWork(w http.ResponseWriter, r *http.Request)
}
type router struct {
	use usecase.Task
}

func NewRouter(use usecase.Task) Router {
	return &router{
		use: use,
	}
}

func (r *router) HomeWork(writer http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		http.Error(writer, "405 Method Not Allowed ", http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(req.Body)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	list, err := dto.NewList(body)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	if err = list.Validate(); err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := r.use.Processing(list)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	_, _ = writer.Write(response)
}

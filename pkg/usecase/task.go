package usecase

import (
	"errors"
	"strings"
	"sync"

	"gitlab.com/stella/pkg/domain"
	"gitlab.com/stella/pkg/dto"
)

type Task interface {
	Processing(list dto.URLList) ([]byte, error)
}

type task struct {
	entity domain.Parser
}

func NewTask(dm domain.Parser) Task {
	return &task{
		entity: dm,
	}
}

func (t *task) Processing(list dto.URLList) ([]byte, error) {
	outError := make([]string, 0)

	wg := &sync.WaitGroup{}

	responseList := make([]string, list.Size())

	for i, uri := range list.List() {
		wg.Add(1)

		go func(index int, url string) {
			defer wg.Done()

			data, err := t.entity.Data(url)
			if err != nil {
				outError = append(outError, err.Error())
				return
			}

			responseList[index] = data
		}(i, uri)
	}

	wg.Wait()

	if len(outError) > 0 {
		return nil, errors.New(strings.Join(outError, ","))
	}

	return []byte(strings.Join(responseList, "\n")), nil
}

package dto

import (
	"errors"
	"net/url"
	"strings"
)

const (
	maxLengthURL = 2048
)

type URLList interface {
	Validate() error
	List() []string
	Size() int
}
type urlList struct {
	list []string
}

func NewList(body []byte) (URLList, error) {
	if len(body) == 0 {
		return nil, errors.New("body is empty, URL list required")
	}

	list := strings.Split(string(body), "\n")

	if len(list) == 0 {
		return nil, errors.New("can't split body")
	}

	return &urlList{list: list}, nil
}

func (l *urlList) List() []string {
	return l.list
}

func (l *urlList) Size() int {
	return len(l.list)
}

func (l *urlList) Validate() error {
	for i := range l.list {
		u, err := url.ParseRequestURI(l.list[i])
		if err != nil {
			return err
		}

		if len(u.String()) > maxLengthURL {
			return errors.New("URL length exceeded 2048 characters")
		}
	}

	return nil
}

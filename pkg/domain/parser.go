package domain

import (
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	timeout = 3 * time.Second
	base10  = 10
)

type Parser interface {
	Data(url string) (string, error)
}

type parser struct {
	client http.Client
}

func NewParser() Parser {
	return &parser{
		client: http.Client{
			Timeout: timeout,
		},
	}
}

func (p *parser) Data(url string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, url, http.NoBody)
	if err != nil {
		return "", err
	}

	response, err := p.client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return "", err
	}
	defer response.Body.Close()

	data, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	out := strconv.FormatInt(int64(len(data)), base10)

	return out, nil
}

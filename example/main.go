package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/stella/pkg/controller"
	"gitlab.com/stella/pkg/domain"
	"gitlab.com/stella/pkg/usecase"
)

const (
	maxConns = 1
	timeout  = 3 * time.Second
)

func main() {
	log.SetFlags(log.Lshortfile)

	task := controller.NewRouter(usecase.NewTask(domain.NewParser()))
	handler := controller.NewLimitHandler(maxConns)
	handler.HandleFunc("/", task.HomeWork)

	srv := http.Server{
		Addr:        ":8080",
		Handler:     handler.MiddleWare(handler),
		ReadTimeout: timeout,
	}

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err.Error())
	}
}
